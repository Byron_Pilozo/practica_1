package byronpilozo.facci.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

Button buttonLogin, buttonBuscar, buttonGuardar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLogin = findViewById(R.id.buttonLogin);
        buttonBuscar = findViewById(R.id.buttonBuscar);
        buttonGuardar= findViewById(R.id.buttonGuardar);

        buttonLogin.setOnClickListener(new View.OnClickListener(){

    @Override
    public void onClick(View v) {
       Intent intent = new Intent(
       MainActivity.this, ActividadLogin.class);
      startActivity(intent);
        }
      });
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, ActividadBuscar.class);
                startActivity(intent);
            }
        });
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, ActividadGuardar.class);
                startActivity(intent);
            }
        });

    }
}

